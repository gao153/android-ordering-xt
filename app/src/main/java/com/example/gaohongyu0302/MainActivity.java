package com.example.gaohongyu0302;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gaohongyu0302.dao.UserDao;
import com.example.gaohongyu0302.entity.User;
import com.example.gaohongyu0302.utils.MD5Utils;
import com.example.gaohongyu0302.utils.SPSave;

import java.util.Map;

/**
 * function：连接页面加载首页
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "mysql-party-MainActivity";
    EditText EditTextAccount;
    EditText EditTextPassword;
    CheckBox save_pwd;
    private String userName,passWord,spPsw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        save_pwd = findViewById(R.id.save_pwd);
        EditTextAccount = findViewById(R.id.uesrAccount);
        EditTextPassword = findViewById(R.id.userPassword);
        save_pwd = findViewById(R.id.save_pwd);
        Map<String ,String> userInfo = SPSave.getUserInfo(this);
        if(userInfo!=null){
            EditTextAccount.setText(userInfo.get("account"));
            EditTextPassword.setText(userInfo.get("password"));
        }

    }
    private void remember(){
        String account = EditTextAccount.getText().toString().trim();
        String password = EditTextPassword.getText().toString().trim();
        SPSave.saveUserInfo(MainActivity.this,account,password);

    }

    public void login(View view) {

        userName = EditTextAccount.getText().toString();
        passWord = EditTextPassword.getText().toString();
        //md5加密
        if(save_pwd.isChecked()){
            remember();
        }else{
            String account = "";
            String password = "";
            SPSave.saveUserInfo(MainActivity.this,account,password);
        }
        String md5Psw = MD5Utils.md5(passWord);
        // 定义方法 readPsw为了读取用户名，得到密码
        System.out.println(md5Psw);
        new Thread() {
            @Override
            public void run() {
                UserDao userDao = new UserDao();
                User user = userDao.login(userName,md5Psw);

                if (user != null) {
                    System.out.println(user.getZid() + "桌");
                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    intent.putExtra("zid", "" + user.getZid());
                    intent.putExtra("name", "");
                    startActivityIfNeeded(intent, 1);
                } else
                    hand1.sendEmptyMessage(0);
            }
        }.start();
    }
//
    @SuppressLint("HandlerLeak")
    final Handler hand1 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0){
                Toast.makeText(getApplicationContext(), "登录失败", Toast.LENGTH_LONG).show();
            } else if (msg.what == 1) {
                Toast.makeText(getApplicationContext(), "登录成功", Toast.LENGTH_LONG).show();
            } else if (msg.what == 2){
                Toast.makeText(getApplicationContext(), "密码错误", Toast.LENGTH_LONG).show();
            } else if (msg.what == 3){
                Toast.makeText(getApplicationContext(), "账号不存在", Toast.LENGTH_LONG).show();
            }
        }
    };
}