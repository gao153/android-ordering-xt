package com.example.gaohongyu0302.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

public class SPSave {
    public  static  boolean saveUserInfo(Context context,String account,String password){
        SharedPreferences sharedPreferences = context.getSharedPreferences("userInfo",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("account",account);
        editor.putString("password",password);
        editor.commit();


        return true;
    }

    public  static Map<String ,String> getUserInfo(Context context){
        Map<String ,String> userMap = new HashMap<>();
        SharedPreferences sp = context.getSharedPreferences("userInfo",Context.MODE_PRIVATE);
        String account = sp.getString("account","");
        String password = sp.getString("password","");
        userMap.put("account",account);
        userMap.put("password",password);
        return userMap;
    }
}
