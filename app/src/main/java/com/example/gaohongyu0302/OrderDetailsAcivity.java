package com.example.gaohongyu0302;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gaohongyu0302.dao.CommodityDao;
import com.example.gaohongyu0302.dao.OrderDetailsDao;
import com.example.gaohongyu0302.entity.Commodity;
import com.example.gaohongyu0302.entity.OrderDetails;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailsAcivity extends AppCompatActivity {
    private ArrayList<OrderDetails> orderDetailsList;
    private String zid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_orderdetails);
        ListView mLv=findViewById(R.id.lv);
        TextView shop=findViewById(R.id.shop);
        OrderDetailsDao orderDetailsDao=new OrderDetailsDao();
        Intent intent=getIntent();
        String name= intent.getStringExtra("name");
        zid= intent.getStringExtra("zid");
        shop.setText(zid+"桌");


        orderDetailsList = orderDetailsDao.OrderDetailsList(zid);

        GoodsAdpter goodsAdpter=new GoodsAdpter();
        mLv.setAdapter(goodsAdpter);
        ListView mListView = (ListView) findViewById(R.id.lv);
//        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                // 第二步：通过Intent跳转至新的页面
//                Intent intent = new Intent(HomeActivity.this, DetailsActivity.class);
//                intent.putExtra("id",commodityList.get(position).getId()+"");
//                intent.putExtra("zid",zid+"");
//                startActivity(intent);
//            }
//        });
    }
    //跳转
    public void search(View view){
        TextView shop=findViewById(R.id.shop);
        Intent intent = new Intent(OrderDetailsAcivity.this, Search.class);
        intent.putExtra("zid", ""+zid);
        System.out.println(zid);

        startActivityIfNeeded(intent, 1);
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }
    //跳转我
    public void wo(View view){
        Intent intent = new Intent(OrderDetailsAcivity.this, UserAcivity.class);
        intent.putExtra("zid", ""+zid);

        startActivityIfNeeded(intent, 1);
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }
    public void menu(View view){
        // 第二步：通过Intent跳转至新的页面
        Intent intent = new Intent(OrderDetailsAcivity.this, HomeActivity.class);
        intent.putExtra("name","");
        intent.putExtra("zid",zid+"");
        startActivity(intent);
    }

    private  class GoodsAdpter  extends BaseAdapter {


        @Override
        public int getCount() {
            return orderDetailsList.size();
        }

        @Override
        public Object getItem(int position) {
            return orderDetailsList.get(position).getCname();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View converView, ViewGroup parent) {
            ViewHolder holder;

//            if(converView==null){
//                converView = View.inflate(HomeActivity.this,R.layout.list_item,null);
//                holder = new ViewHolder();
//                holder.title=converView.findViewById(R.id.title);
//                holder.price=converView.findViewById(R.id.price);
//                holder.image=converView.findViewById(R.id.iv_item);
//
//            }
//            return null;

            View view=View.inflate(OrderDetailsAcivity.this,R.layout.list_item,null);
            TextView title=view.findViewById(R.id.title);
            TextView price=view.findViewById(R.id.price);
            ImageView image=view.findViewById(R.id.iv_item);
            title.setText(orderDetailsList.get(position).getCname());
            price.setText("1份           "+orderDetailsList.get(position).getPrices()+"元");

            image.setBackgroundResource(orderDetailsList.get(position).getCphoto());
//            image.setBackgroundResource(R.drawable.y);
            return view;
        }
    }
    private class ViewHolder{
        private  TextView title;
        private  TextView price;
        private ImageView image;

    }

}
