package com.example.gaohongyu0302;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gaohongyu0302.dao.CommodityDao;

public class Search extends AppCompatActivity {
    private static final String TAG = "mysql-party-Search";
    String zid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search);

        Intent intent=getIntent();
        zid= intent.getStringExtra("zid");
    }
    public void search(View view){
        EditText EditTextName = findViewById(R.id.name);

        Intent intent = new Intent(Search.this, HomeActivity.class);
        intent.putExtra("name",EditTextName.getText().toString().trim());
        intent.putExtra("zid",zid);
        startActivityIfNeeded(intent, 2);
    }


}
