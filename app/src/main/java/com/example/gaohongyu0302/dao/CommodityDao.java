package com.example.gaohongyu0302.dao;

import android.util.Log;

import com.example.gaohongyu0302.entity.Commodity;
import com.example.gaohongyu0302.entity.Order;
import com.example.gaohongyu0302.entity.User;
import com.example.gaohongyu0302.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommodityDao {

    private static final String TAG = "mysql-party-CommodityDao";
//查询所有菜品
    public ArrayList<Commodity> CommodityList(String referred){
        ArrayList<Commodity> list=new ArrayList<Commodity>();

        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();
        try {
            // mysql简单的查询语句。这里是根据user表的userAccount字段来查询某条记录
            String sql="";
                sql = "SELECT * FROM `commodity`C  WHERE `referred` LIKE ?";


            if (connection != null){// connection不为null表示与数据库建立了连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){
                    Log.e(TAG,"搜索：" + referred);
                    //根据账号进行查询
                    System.out.println(ps);
                        ps.setString(1,"%" +referred+"%");

                    // 执行sql查询语句并返回结果集
                    ResultSet rs = ps.executeQuery();
                    int count = rs.getMetaData().getColumnCount();
                    //将查到的内容储存在map里
                    while (rs.next()){
                        Commodity commodity=new Commodity();
                        // 注意：下标是从1开始的
                        int id=rs.getInt(1);
                        String stname=rs.getString(2);
                        int photo=rs.getInt(4);
                        int inventory=rs.getInt(6);
                        double price=rs.getDouble(7);
                        commodity.setId(id);
                        commodity.setName(stname);
                        commodity.setPhoto(photo);
                        commodity.setInventory(inventory);
                        commodity.setPrice(price);
                        System.out.println(commodity.toString());
                        list.add(commodity);
                    }
                    System.out.println(list.size());

                    return list;
                }else {
                    return null;
                }
            }else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "异常搜索：" + e.getMessage());
            return null;
        }

    }
//详情
    public Commodity getCommodityById(int id){
        Commodity commodity=new Commodity();

        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();
        try {
            // mysql简单的查询语句。这里是根据user表的userAccount字段来查询某条记录
            String sql="";
            sql = "SELECT * FROM `commodity`  WHERE `id` = ?";


            if (connection != null){// connection不为null表示与数据库建立了连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){
                    Log.e(TAG,"详情" + id);
                    //根据账号进行查询
                    System.out.println(ps);
                    ps.setString(1,id+"");

                    // 执行sql查询语句并返回结果集
                    ResultSet rs = ps.executeQuery();
                    int count = rs.getMetaData().getColumnCount();
                    //将查到的内容储存在map里
                    while (rs.next()){
                        // 注意：下标是从1开始的
                        String name=rs.getString(2);
                        int photo=rs.getInt(4);//照片
                        String details=rs.getString(5);//详情
                        int inventory=rs.getInt(6);
                        double price=rs.getDouble(7);
                        int sales=rs.getInt(8);//销量
                        commodity.setId(id);
                        commodity.setName(name);
                        commodity.setPhoto(photo);
                        commodity.setDetails(details);
                        commodity.setInventory(inventory);
                        commodity.setPrice(price);
                        commodity.setSales(sales);
                        System.out.println(commodity.toString());
                    }

                    return commodity;
                }else {
                    return null;
                }
            }else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "异常搜索：" + e.getMessage());
            return null;
        }

    }
//修改库存，销量
    public boolean updateCommodity(int id){
        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();

        try {
            String sql = "UPDATE commodity SET inventory = inventory-1, sales = sales+1 WHERE id = ?";
            if (connection != null){// connection不为null表示与数据库建立了sales连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){

                    //将数据插入数据库
                    ps.setInt(1,id);

                    // 执行sql查询语句并返回结果集
                    int rs = ps.executeUpdate();
                    if(rs>0)
                        return true;
                    else
                        return false;
                }else {
                    return  false;
                }
            }else {
                return  false;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "异常updateCommodity：" + e.getMessage());
            return false;
        }

    }
}
