package com.example.gaohongyu0302.dao;

import android.util.Log;

import com.example.gaohongyu0302.entity.User;
import com.example.gaohongyu0302.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 * author: yan
 * date: 2022.02.17
 * **/
public class UserDao{

    private static final String TAG = "mysql-party-UserDao";

    /**
     * function: 登录
     * */
    public User login(String userAccount, String userPassword){
        User user=new User();
        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();
        int msg = 0;
        try {
            // mysql简单的查询语句。这里是根据user表的userAccount字段来查询某条记录
            String sql = "SELECT * FROM `user` WHERE`uname`= ? AND `password` =?";
            if (connection != null){// connection不为null表示与数据库建立了连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){
                    Log.e(TAG,"账号：" + userAccount);
                    //根据账号进行查询
                    System.out.println(ps);
                    ps.setString(1, userAccount);
                    ps.setString(2, userPassword);
                    // 执行sql查询语句并返回结果集
                    ResultSet rs = ps.executeQuery();
                    int count = 0;
                    //将查到的内容储存在map里

                        while (rs.next()) {
                            // 注意：下标是从1开始的
                            String name = rs.getString(2);
                            int zid = rs.getInt(5);
                            user.setZid(zid);
                            user.setUname(name);
                            count=1;
                        }
                    if(count==0){
                        return null;
                    }
                    connection.close();
                    ps.close();
                    System.out.println(user.toString());
                    return user;
                }else {
                    msg = 0;
                }
            }else {
                msg = 0;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "异常login：" + e.getMessage());
            return null;
        }
        return null;
    }


    /**
     * function: 注册
     * */
    public boolean insertUser(String name,String password,String zid){
        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();

        try {
            String sql = "insert into `user` (`uname`,password,zid) values (?,?,?)";
            if (connection != null){// connection不为null表示与数据库建立了sales连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){

                    //将数据插入数据库
                    ps.setString(1,name);
                    ps.setString(2,password);
                    ps.setString(3,zid);

                    // 执行sql查询语句并返回结果集
                    int rs = ps.executeUpdate();
                    if(rs>0)
                        return true;
                    else
                        return false;
                }else {
                    return  false;
                }
            }else {
                return  false;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "异常updateUserByPassword：" + e.getMessage());
            return false;
        }

    }

    /**
     * function: 修改密码
     * */
    public boolean updateUserByPassword(String name,String password){
        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();

        try {
            String sql = "UPDATE `user` SET `password` = ? WHERE `uname` = ?";
            if (connection != null){// connection不为null表示与数据库建立了sales连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){

                    //将数据插入数据库
                    ps.setString(1,password);
                    ps.setString(2,name);

                    // 执行sql查询语句并返回结果集
                    int rs = ps.executeUpdate();
                    if(rs>0)
                        return true;
                    else
                        return false;
                }else {
                    return  false;
                }
            }else {
                return  false;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "异常updateUserByPassword：" + e.getMessage());
            return false;
        }

    }

}
