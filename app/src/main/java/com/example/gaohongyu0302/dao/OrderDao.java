package com.example.gaohongyu0302.dao;

import android.util.Log;

import com.example.gaohongyu0302.entity.Order;
import com.example.gaohongyu0302.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class OrderDao {
    private static final String TAG = "mysql-party-OrderDao";

    public boolean insertOrder(Order order){
        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();


        try {
            String sql = "insert into `order` (`uzid`, `orderDate`, `prices`) values (?, ?, ?)";
            if (connection != null){// connection不为null表示与数据库建立了连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){

                    //将数据插入数据库
                    ps.setInt(1,order.getUzid());
                    ps.setString(2,order.getOrderDate()+"");
                    ps.setString(3,order.getPrices()+"");

                    // 执行sql查询语句并返回结果集
                    int rs = ps.executeUpdate();
                    if(rs>0)
                        return true;
                    else
                        return false;
                }else {
                    return  false;
                }
            }else {
                return  false;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "异常register：" + e.getMessage());
            return false;
        }

    }
}
