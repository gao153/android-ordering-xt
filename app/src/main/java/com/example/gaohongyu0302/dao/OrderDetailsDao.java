package com.example.gaohongyu0302.dao;

import android.util.Log;

import com.example.gaohongyu0302.entity.Commodity;
import com.example.gaohongyu0302.entity.Order;
import com.example.gaohongyu0302.entity.OrderDetails;
import com.example.gaohongyu0302.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class OrderDetailsDao {
    private static final String TAG = "mysql-party-OrderDetailsDao";
//添加
    public boolean insertOrderDetails(OrderDetails orderDetails){
        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();


        try {
            String sql = "insert into `orderdetails` (`cid`, `cname`, `cphoto`,price) values (?, ?, ?,?)";
            if (connection != null){// connection不为null表示与数据库建立了连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){

                    //将数据插入数据库
                    ps.setInt(1,orderDetails.getCid());
                    ps.setString(2,orderDetails.getCname());
                    ps.setInt(3,orderDetails.getCphoto());
                    ps.setDouble(4,orderDetails.getPrices());

                    // 执行sql查询语句并返回结果集
                    int rs = ps.executeUpdate();
                    if(rs>0)
                        return true;
                    else
                        return false;
                }else {
                    return  false;
                }
            }else {
                return  false;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "异常insertOrderDetails：" + e.getMessage());
            return false;
        }

    }
    public ArrayList<OrderDetails> OrderDetailsList(String zid){
        ArrayList<OrderDetails> list=new ArrayList<OrderDetails>();

        // 根据数据库名称，建立连接
        Connection connection = JDBCUtils.getConn();
        try {
            // mysql简单的查询语句。这里是根据user表的userAccount字段来查询某条记录
            String sql="";
            sql = "SELECT * FROM `orderdetails` ,`order` WHERE `orderdetails`.`orderNum`=`order`.`orderNum` AND `order`.`quantity` =1 AND uzid= ?";


            if (connection != null){// connection不为null表示与数据库建立了连接
                PreparedStatement ps = connection.prepareStatement(sql);
                if (ps != null){
                    Log.e(TAG,"搜索：" + zid);
                    //根据账号进行查询
                    System.out.println(ps);
                    ps.setString(1,zid+"");

                    // 执行sql查询语句并返回结果集
                    ResultSet rs = ps.executeQuery();
                    //将查到的内容储存在map里
                    while (rs.next()){
                        // 注意：下标是从1开始的
                        int cid=rs.getInt(2);
                        String cname=rs.getString(3);
                        int cphoto=rs.getInt(4);
                        double price=rs.getDouble(5);
                        OrderDetails orderDetails=new OrderDetails(cid,cname,cphoto,price);
                        list.add(orderDetails);
                    }
                    System.out.println(list.size());

                    return list;
                }else {
                    return null;
                }
            }else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "异常搜索：" + e.getMessage());
            return null;
        }

    }
}
