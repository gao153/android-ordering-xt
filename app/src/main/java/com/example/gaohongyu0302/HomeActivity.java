package com.example.gaohongyu0302;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gaohongyu0302.dao.CommodityDao;
import com.example.gaohongyu0302.entity.Commodity;

import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private List<Commodity> commodityList;
    private String zid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ListView mLv=findViewById(R.id.lv);
        TextView shop=findViewById(R.id.shop);
        CommodityDao commodityDao=new CommodityDao();
        Intent intent=getIntent();
        String name= intent.getStringExtra("name");
        zid= intent.getStringExtra("zid");
        shop.setText(zid+"桌");


        commodityList = commodityDao.CommodityList(name);

        GoodsAdpter goodsAdpter=new GoodsAdpter();
        mLv.setAdapter(goodsAdpter);
        ListView mListView = (ListView) findViewById(R.id.lv);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // 第二步：通过Intent跳转至新的页面
                Intent intent = new Intent(HomeActivity.this, DetailsActivity.class);
                intent.putExtra("id",commodityList.get(position).getId()+"");
                intent.putExtra("zid",zid+"");
                startActivity(intent);
            }
        });
    }
    //跳转
    public void search(View view){
        Intent intent = new Intent(HomeActivity.this, Search.class);
        intent.putExtra("zid", ""+zid);
        System.out.println(zid);

        startActivityIfNeeded(intent, 1);
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }
    //跳转我
    public void wo(View view){
        Intent intent = new Intent(HomeActivity.this, UserAcivity.class);
        intent.putExtra("zid", ""+zid);
        System.out.println("家数据库链接离开的撒娇雷克萨的结课了仨多加了卡萨丁家里开");

        startActivityIfNeeded(intent, 1);
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }
    public void OrderDetailsList(View view){
        Intent intent = new Intent(HomeActivity.this, OrderDetailsAcivity.class);
        intent.putExtra("zid", ""+zid);
        System.out.println(zid);

        startActivityIfNeeded(intent, 1);
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }
    public void menu(View view){
        ListView mLv=findViewById(R.id.lv);
        CommodityDao commodityDao=new CommodityDao();
        commodityList = commodityDao.CommodityList("");

        GoodsAdpter goodsAdpter=new GoodsAdpter();
        mLv.setAdapter(goodsAdpter);
    }
    private  class GoodsAdpter  extends BaseAdapter {


        @Override
        public int getCount() {
            return commodityList.size();
        }

        @Override
        public Object getItem(int position) {
            return commodityList.get(position).getName();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View converView, ViewGroup parent) {
            ViewHolder holder;

//            if(converView==null){
//                converView = View.inflate(HomeActivity.this,R.layout.list_item,null);
//                holder = new ViewHolder();
//                holder.title=converView.findViewById(R.id.title);
//                holder.price=converView.findViewById(R.id.price);
//                holder.image=converView.findViewById(R.id.iv_item);
//
//            }
//            return null;

            View view=View.inflate(HomeActivity.this,R.layout.list_item,null);
            TextView title=view.findViewById(R.id.title);
            TextView price=view.findViewById(R.id.price);
            ImageView image=view.findViewById(R.id.iv_item);
            title.setText(commodityList.get(position).getName());
            price.setText(commodityList.get(position).getPrice()+"元/份");

            image.setBackgroundResource(commodityList.get(position).getPhoto());
            return view;
        }
    }
    private class ViewHolder{
        private  TextView title;
        private  TextView price;
        private ImageView image;

    }
}