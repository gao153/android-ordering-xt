package com.example.gaohongyu0302;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gaohongyu0302.dao.CommodityDao;
import com.example.gaohongyu0302.dao.OrderDao;
import com.example.gaohongyu0302.dao.OrderDetailsDao;
import com.example.gaohongyu0302.entity.Commodity;
import com.example.gaohongyu0302.entity.Order;
import com.example.gaohongyu0302.entity.OrderDetails;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailsActivity  extends AppCompatActivity {
    private static final String TAG = "mysql-party-Details";
    private int zid;
    private Commodity commodity;
    private CommodityDao commodityDao=new CommodityDao();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_details);

        Intent intent=getIntent();
        zid= Integer.parseInt(intent.getStringExtra("zid"));
        int id= Integer.parseInt(intent.getStringExtra("id"));

        commodity=commodityDao.getCommodityById(id);
        ImageView imageView=findViewById(R.id.caiping);
        TextView name=findViewById(R.id.tvname);
        TextView price=findViewById(R.id.tvPrice);
        TextView Sales=findViewById(R.id.tvSales);
        TextView details=findViewById(R.id.tvDetails);
        TextView inventory=findViewById(R.id.tvinventory);
        imageView.setBackgroundResource(commodity.getPhoto());
        name.setText(commodity.getName());
        price.setText(commodity.getPrice()+"元");
        Sales.setText(commodity.getSales()+"份");
        details.setText(commodity.getDetails());
        inventory.setText(commodity.getInventory()+"");
    }
    public void fanhui(View view){
        Intent intent = new Intent(DetailsActivity.this, HomeActivity.class);
        intent.putExtra("zid", ""+zid);
        intent.putExtra("name", "");
        startActivity(intent);
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }

    public void place(View view){
        Date date = new Date();
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        System.out.println(dateFormat.format(date));
        Intent intent = new Intent(DetailsActivity.this, OrderActivity.class);
        intent.putExtra("zid", ""+zid);
        OrderDao orderDao=new OrderDao();
        OrderDetailsDao orderDetailsDao=new OrderDetailsDao();
        Order order=new Order(zid,dateFormat.format(date),commodity.getPrice());
        OrderDetails orderDetails=new OrderDetails(commodity.getId(),commodity.getName(),commodity.getPhoto(),commodity.getPrice());
        boolean rs=orderDao.insertOrder(order);
        boolean rsOrderDetails=orderDetailsDao.insertOrderDetails(orderDetails);
        boolean rsupdateCommodity=commodityDao.updateCommodity(commodity.getId());
        if (rs&&rsOrderDetails&&rsupdateCommodity){
            startActivity(intent);
        }
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }

}
