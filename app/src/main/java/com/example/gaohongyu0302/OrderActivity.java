package com.example.gaohongyu0302;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gaohongyu0302.entity.Commodity;

public class OrderActivity extends AppCompatActivity {
    private static final String TAG = "mysql-party-Details";
    String zid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_order);
        Intent intent=getIntent();
        zid= intent.getStringExtra("zid");
        TextView tv=findViewById(R.id.text);
        tv.setText("下单成功");
    }
    public void caidan(View view){
        EditText EditTextName = findViewById(R.id.name);

        Intent intent = new Intent(OrderActivity.this, HomeActivity.class);
        intent.putExtra("name","");
        intent.putExtra("zid",zid);
        startActivityIfNeeded(intent, 2);
    }
}
