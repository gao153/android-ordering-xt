package com.example.gaohongyu0302.entity;

import java.util.Date;

public class Commodity {
    private int id;
    private String name;
    private String referred;
    private int photo;
    private String details;
    private int inventory;
    private double price;
    private int sales;
    private int put;
    private Date time;

    @Override
    public String toString() {
        return "Commodity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", photo='" + photo + '\'' +
                ", details='" + details + '\'' +
                ", inventory=" + inventory +
                ", price=" + price +
                ", sales=" + sales +
                ", put=" + put +
                ", time=" + time +
                '}';
    }

    public Commodity() {
    }

    public Commodity(int id, String name, int photo, String details, int inventory, double price, int sales, int put, Date time) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.details = details;
        this.inventory = inventory;
        this.price = price;
        this.sales = sales;
        this.put = put;
        this.time = time;
    }

    public String getReferred() {
        return referred;
    }

    public void setReferred(String referred) {
        this.referred = referred;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public int getPut() {
        return put;
    }

    public void setPut(int put) {
        this.put = put;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
