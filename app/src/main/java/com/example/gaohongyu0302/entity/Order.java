package com.example.gaohongyu0302.entity;

import java.util.Date;

public class Order {
    private int orderNum;
    private int uzid;
    private String orderDate;
    private double prices;
    private int quantity;

    public Order(int uzid, String orderDate, double prices) {
        this.uzid = uzid;
        this.orderDate = orderDate;
        this.prices = prices;
    }


    @Override
    public String toString() {
        return "Order{" +
                "orderNum=" + orderNum +
                ", uzid=" + uzid +
                ", orderDate=" + orderDate +
                ", prices=" + prices +
                ", quantity=" + quantity +
                '}';
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public int getUzid() {
        return uzid;
    }

    public void setUzid(int uzid) {
        this.uzid = uzid;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public double getPrices() {
        return prices;
    }

    public void setPrices(double prices) {
        this.prices = prices;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
