package com.example.gaohongyu0302.entity;

public class OrderDetails {
    private int orderNum;
    private int cid;
    private String cname;
    private int cphoto;
    private double prices;
    private int quantity;

    public OrderDetails(int cid, String cname, int cphoto, double prices) {
        this.cid = cid;
        this.cname = cname;
        this.cphoto = cphoto;
        this.prices = prices;
    }

    public OrderDetails() {
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public int getCphoto() {
        return cphoto;
    }

    public void setCphoto(int cphoto) {
        this.cphoto = cphoto;
    }

    public double getPrices() {
        return prices;
    }

    public void setPrices(double prices) {
        this.prices = prices;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
