package com.example.gaohongyu0302;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gaohongyu0302.dao.CommodityDao;

public class UserAcivity  extends AppCompatActivity {
    private static final String TAG = "mysql-party-Search";
    String zid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user);

        Intent intent=getIntent();
        zid= intent.getStringExtra("zid");
    }

//注册
    public void register(View view){
        EditText EditTextName = findViewById(R.id.name);

        Intent intent = new Intent(UserAcivity.this, RegisterAcivity.class);
        intent.putExtra("name","");
        intent.putExtra("zid",zid);
        startActivityIfNeeded(intent, 2);
    }

    //修改密码
    public void updatePassword(View view){
        EditText EditTextName = findViewById(R.id.name);

        Intent intent = new Intent(UserAcivity.this, UpdatePasswordAcivity.class);
        intent.putExtra("name","");
        intent.putExtra("zid",zid);
        startActivityIfNeeded(intent, 2);
    }
    public void menu(View view){
        EditText EditTextName = findViewById(R.id.name);

        Intent intent = new Intent(UserAcivity.this, HomeActivity.class);
        intent.putExtra("name","");
        intent.putExtra("zid",zid);
        startActivityIfNeeded(intent, 2);
    }
    //跳转
    public void search(View view){
        Intent intent = new Intent(UserAcivity.this, Search.class);
        intent.putExtra("zid", ""+zid);
        System.out.println(zid);

        startActivityIfNeeded(intent, 1);
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }

    public void OrderDetailsList(View view){
        Intent intent = new Intent(UserAcivity.this, OrderDetailsAcivity.class);
        intent.putExtra("zid", ""+zid);
        System.out.println(zid);

        startActivityIfNeeded(intent, 1);
//        startActivity(new Intent(getApplicationContext(),Search.class));
    }
}

