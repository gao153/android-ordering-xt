package com.example.gaohongyu0302;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gaohongyu0302.dao.UserDao;
import com.example.gaohongyu0302.entity.User;
import com.example.gaohongyu0302.utils.MD5Utils;

public class RegisterAcivity extends AppCompatActivity {
    private static final String TAG = "mysql-party-RegisterAcivity";
    String zid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);

        Intent intent=getIntent();
        zid= intent.getStringExtra("zid");
    }
    public void register(View view){
        EditText uname = findViewById(R.id.reRdmin);
        EditText password = findViewById(R.id.rePassword);
        EditText zid = findViewById(R.id.table);
        new Thread() {
            @Override
            public void run() {
                //md5加密
                String passWord=password.getText().toString();
                String md5Psw = MD5Utils.md5(passWord);
                UserDao userDao = new UserDao();
                boolean rs=userDao.insertUser(uname.getText().toString(),md5Psw,zid.getText().toString());
                if (rs) {
                        hand1.sendEmptyMessage(2);
                } else
                    hand1.sendEmptyMessage(0);
            }
        }.start();
    }

    @SuppressLint("HandlerLeak")
    final Handler hand1 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0){
                Toast.makeText(getApplicationContext(), "注册失败！", Toast.LENGTH_LONG).show();
            } else if (msg.what == 2) {
                Toast.makeText(getApplicationContext(), "注册成功！", Toast.LENGTH_LONG).show();
            }
        }
    };
    public void login(View view){
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }
}
